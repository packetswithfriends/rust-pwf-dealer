use zmq;
use std::thread;

fn pull(puller: &zmq::Socket, workers: &zmq::Socket) {
/*    loop {
        loop {
            let message = puller.recv_msg(0).unwrap();
            println!("{:?}", &message);
            let more = message.get_more();
            workers
                .send(message, if more { zmq::SNDMORE } else { 0 })
                .unwrap();
            if !more {
                break;
            }
        }
    }*/
    zmq::proxy(&puller, &workers).expect("failed to proxy");
}

fn publish(puller: &zmq::Socket, publisher: &zmq::Socket) {
    loop {
        publisher.send("pwfparsed", zmq::SNDMORE).unwrap();

        loop {
            let message = puller.recv_msg(0).unwrap();
            let more = message.get_more();
            publisher
                .send(message, if more { zmq::SNDMORE } else { 0 })
                .unwrap();
            if !more {
                break;
            }
        }
    }
}

fn main() {
    let context = zmq::Context::new();
    let puller = context.socket(zmq::PULL).unwrap();
    puller
        .bind("tcp://*:5555")
        .expect("puller socket failed");
    let workerpusher = context.socket(zmq::PUSH).unwrap();
    workerpusher
        .bind("ipc:///tmp/pwf_dealer_distrib.sock")
        .expect("worker pusher socket failed");

    let pull_thread = thread::spawn(move || pull(&puller, &workerpusher));


    let workerpuller = context.socket(zmq::PULL).unwrap();
    workerpuller
        .bind("ipc:///tmp/pwf_dealer_collect.sock")
        .expect("worker puller socket failed");
    let publisher = context.socket(zmq::PUB).unwrap();
    publisher
        .bind("tcp://*:5066")
        .expect("publisher bind failed");

    let pub_thread = thread::spawn(move || publish(&workerpuller, &publisher));

    pull_thread.join().unwrap();
    pub_thread.join().unwrap();
}
